#include"cbuffer.h"

void cb_init(cbuffer_t* cb, void* buf, uint32_t size)
{
	cb->data = (uint8_t*)buf;
	cb->active = true;
	cb->size = size;
	cb->reader = 0;
	cb->writer = 0;
	cb->overflow = 0;
}
void cb_clear(cbuffer_t* cb)
{
	free(cb);
}
uint32_t cb_read(cbuffer_t* cb, void* buf, uint32_t nbytes)
{

	for (int i = 0; i < nbytes; i++)
	{
		if (cb->reader == cb->size)
		{
			cb->reader = 0;
        }
		*((uint8_t*)buf + i) = cb->data[cb->reader];
		cb->reader++;
	}
	return cb->reader;

}
uint32_t cb_write(cbuffer_t* cb, void* buf, uint32_t nbytes)
{
	for (int i = 0; i < nbytes; i++)
	{
		if (cb->writer == cb->size)
		{
			cb->overflow ++;
			printf("\nOverlow Cbuffer:  %d", cb->overflow);
			cb->writer  = 0;
		}
		cb->data[cb->writer] = *((uint8_t*)buf + i);
        cb->writer++;
	}
	return cb->writer;
}
uint32_t cb_data_count(cbuffer_t* cb)
{
	return cb->writer-cb->reader;
}
uint32_t cb_space_count(cbuffer_t* cb)
{
	return cb->size - cb_data_count(cb);
}